import { MooreknightsPage } from './app.po';

describe('mooreknights App', function() {
  let page: MooreknightsPage;

  beforeEach(() => {
    page = new MooreknightsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
