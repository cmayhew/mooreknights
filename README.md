# Tech Knights CI/CD Workshop

## This project has 3 deployment environments in S3
[DEV](http://dev-knights.com.s3-website-us-east-1.amazonaws.com/)

[QA/UAT](http://qa-knights.com.s3-website-us-east-1.amazonaws.com/)

[PROD](http://knights.com.s3-website-us-east-1.amazonaws.com/)

## To participate in the workshop

Create an account on [BitBucket](https://bitbucket.com)

Join the Slack Channel - [knightsdemo.slack.com](https://knightsdemo.slack.com/)

Shoot me your email so I can add you to the JIRA project 

## To Clone and Run this project install Angular CLI

# Installation

**BEFORE YOU INSTALL:** please read the [prerequisites](#prerequisites)
```bash
npm install -g angular-cli
```

## Usage

```bash
ng --help
```

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.19-3.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class`.

## Further help

To get more help on the `angular-cli` use `ng --help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).